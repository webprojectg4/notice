package com.project.web.Services;

import java.util.Optional;



import com.project.web.Announcement;


public interface AnnouncementService {
	void deleteAnnouncement(Long id);
	void saveAnnouncement(Announcement a);
	Optional<Announcement> findAnnouncement(Long id);
	Iterable<Announcement> findAllAnnouncements();
}
