package com.project.web.Services;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.project.web.User;


public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	void saveTeacher(User user);
	List<User> findAllUsersByRole(String role);
	
}