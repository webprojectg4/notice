package com.project.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ItscNoticeBoardApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ItscNoticeBoardApplication.class, args);
	}
	
}

