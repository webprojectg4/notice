package com.project.web.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.project.web.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
	
	
	@Query(value="SELECT * FROM users join roles ON users.id=roles.user_id where roles.user_role  =:role",nativeQuery=true)
	List<User> findAllByRole(String role);
}
