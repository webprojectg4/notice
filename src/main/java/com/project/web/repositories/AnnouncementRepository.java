package com.project.web.repositories;



import org.springframework.data.repository.CrudRepository;

import com.project.web.Announcement;

public interface AnnouncementRepository extends CrudRepository<Announcement, Long> {
		
}
