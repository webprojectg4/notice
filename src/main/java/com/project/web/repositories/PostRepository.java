package com.project.web.repositories;



import org.springframework.data.repository.CrudRepository;

import com.project.web.Post;

public interface PostRepository extends CrudRepository<Post, Long> {

}
