package com.project.web.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.web.User;
import com.project.web.Services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/registerTeacher")
public class RegisterTeacherController {
	
  @Autowired
  private UserService userService;
  
  
  @GetMapping
  public String registerTeacher(Model model,@ModelAttribute User user,@AuthenticationPrincipal UserDetails  userDetails) {
	  
	  model.addAttribute("user",new User());
	  User user1=userService.findUserByUsername(userDetails.getUsername());
		log.info(user1.getFirstName());
      return "registerTeacher";    
  }
  
  @RequestMapping(params="register",method = RequestMethod.POST)
  public String registerUser(@Valid @ModelAttribute("user") User user, Errors errors) {
	 
	  if (errors.hasErrors()) {
		  return "registerTeacher";
	  }
	  
	  userService.saveTeacher(user);
	  
	  log.info("Teacher saved");
		
	  return "redirect:/registerTeacher";
	}
  
  @RequestMapping(params="clear")
  public String clear() {
  	return "redirect:/registerTeacher";
  }
}
