package com.project.web.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.web.Services.PostService;

@Controller 
@RequestMapping("/displayAnnouncement")
public class DisplayAnnouncementController {
	
	@Autowired
	PostService postService;
	
	@GetMapping
	public String displayAnnouncement() {
		return "displayAnnouncement";
	}

}
