INSERT INTO users(username,password) VALUES ('admin','$2a$10$10q7QTIrVJCZUxbZMVrJ6uL/FuPiEG.Jq8upuyAr8Aa6w9oevdTAm');
SET @id := (SELECT id FROM users);
INSERT INTO roles(user_role,user_id) VALUES ('ADMIN',@id);
